


//PTrip = Planned Trip

/*<br>
##########################################################################<br>
-- Name : API's for RideLogik	
-- Date             : 10th April 2015
-- Author           :   Abdul Vajid KT
-- Company          :   stanDaarD-Z.nl<br>
-- Purpose          :   <br>
-- Usage        sqlplus <br>
-- Impact   :<br>
-- Required grants  :   sel on A, upd on B, drop on C<br>
-- Called by        :   some other process<br
##########################################################################<br>
-- ver  user    date        change  <br>
-- 1.0  DDZ 20110622    initial<br>
##########################################################################<br>
*/


//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("createPTrip", function(request, response) {
  

	if(request.user){
  
	  var src_geopoint_planned  = new Parse.GeoPoint(parseFloat(request.params.source.lat), parseFloat(request.params.source.lng));
	  var startDate_planned = new Date("request.params.date.iso");

	  var PlannedTrip = Parse.Object.extend("PlannedTrips");
	  var plannedTrip = new PlannedTrip();

	  plannedTrip.set("trip_name", request.params.tripname);
	  
	  // plannedTrip.set("startDate_planned",startDate_planned);
	  plannedTrip.set("user_obj", request.user);
	  plannedTrip.set("invited_users", request.params.invited_users);
	  plannedTrip.set("accepted_users", [request.user.id]);
	  plannedTrip.set("rejected_users", []);
	  plannedTrip.set("isActive", true);
	  plannedTrip.set("trip_status", 0);
	  
	  plannedTrip.set("src_place_planned", request.params.source.place_name);
	  plannedTrip.set("dest_place_planned", request.params.destination.place_name);
	  plannedTrip.set("waypoints", request.params.waypoints);
	  plannedTrip.set("src_geopoint_planned", src_geopoint_planned);
	  plannedTrip.set("dest_lat_planned", request.params.destination.lat);
	  plannedTrip.set("dest_lon_planned", request.params.destination.lng);
	  plannedTrip.set("dest_place_planned", request.params.destination.place_name);
	  
	  plannedTrip.save(null, {success: function(result_parent){

	  		  var ChildPTrip = Parse.Object.extend("ChildPTrips");
			  var childPTrip = new ChildPTrip();
			  
			  childPTrip.set("pTrip_obj", result_parent);
			  childPTrip.set("user_obj", request.user);
			  childPTrip.set("isUserOwner", true);
			  childPTrip.set("trip_status", 0);
			  childPTrip.save(null, {success: function(result_child){
					
					var obj_response_success = { "pTrip_id" : result_parent.id,"cTrip_id" : result_child.id, "message" : "Trip is Successfully Created !"};
			        response.success(obj_response_success);                                    
			      },
			      error : function(error){                                        
			      		console.log("error is ");
			      	    console.log(error);
			            response.error(error);
		         }		
			 }); //save code ends here 
	        
	      },
	      error : function(error){                                        
	      		console.log("error is ");
	      	    console.log(error);
	            response.error(error);
	        }
	 }); //save code ends here

    }
    else{
   			response.error("You are not authorized to create a trip");
    }

});


Parse.Cloud.afterSave("PlannedTrips", function(request) {

if(!request.object.existed())
{
      var Counter = Parse.Object.extend("Counters");
      var obj_count = new Counter();
      obj_count.id = "421FaJJMbS";

      // Increment the current value of the quantity key by 1
      obj_count.increment("counter");

      // Save
      obj_count.save(null, {
        success: function(obj_count) {
                                
                  console.log("new value to be printed is "+obj_count.get("counter"));              
                  console.log();
                  var new_counter = obj_count.get("counter");                  
                  request.object.set("trip_no",new_counter);
                  request.object.save();
                  //its request.object.id not request.object.get('objectId');
        },
        error: function(count, error) {
                console.log("increment is failure and object is "+count);
                console.log("error details are "+error);      
        }
      });
}//end of IF REQUEST OBJECT EXISTED
  
});  //AfterSave on PlannedTrips



//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("acceptPTrip", function(request, response) {

if(request.user){

	  var ChildPTrip = Parse.Object.extend("ChildPTrips");
	  var childPTrip = new ChildPTrip();
	  
	  childPTrip.set("pTrip_obj", {__type: "Pointer",className: "PlannedTrips",objectId:request.params.pTrip_objid});
	  childPTrip.set("user_obj", request.user);
	  childPTrip.set("isUserOwner", false);
	  childPTrip.set("trip_status", 0);
	  childPTrip.save(null, {success: function(result){

	  		  var PlannedTrip = Parse.Object.extend("PlannedTrips");
		      var pTrip = new PlannedTrip();
		      pTrip.id = request.params.pTrip_objid;

		      pTrip.addUnique("accepted_users",request.user.id);
		      // Save
		      pTrip.save();
			
			var obj_response_success = { "trip_id" : result.id, "message" : "You are now part of Planned Trip. ChildTrip Created"};
	        response.success(obj_response_success);                                    
	        
	      },
	      error : function(error){                                        
	      		console.log("error is ");
	      	    console.log(error);
	            response.error(error);
	        }
	 }); //save code ends here

	}else{
		response.error("You are not a logged in user");
    }  
  
});



//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("startPTrip", function(request, response) {

if(request.user){

	  var user_src_loc  = new Parse.GeoPoint(parseFloat(request.params.lat), parseFloat(request.params.lon));
	  var var_time = new Date();
	  var usertrips_obj = {__type: "Pointer",className: "UserTrips",objectId:request.params.usertrips_obj};
	  var tripvehicle_obj = {__type: "Pointer",className: "UserVehicles",objectId:request.params.trip_vehicle_obj};

	  var ChildPTrip = Parse.Object.extend("ChildPTrips");
	  var childPTrip = new ChildPTrip();
	  	  
	  childPTrip.set("startDate", var_time);
	  childPTrip.set("Uservehicles_obj", tripvehicle_obj);	  
	  childPTrip.set("usertrips_obj", usertrips_obj);	  
	  childPTrip.set("trip_status", 1);
	  childPTrip.save(null, {success: function(result){
			
			var obj_response_success = { "trip_id" : result.id, "message" : "Trip is Successfully Created !"};
	        response.success(obj_response_success);
	        console.log(request.params.date.iso);
	      },
	      error : function(error){                                        
	      		console.log("error is ");
	      	    console.log(error);
	            response.error(error);
	        }
	 }); //save code ends here

}else{
	response.error("You are not a logged in user");
}

});


//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("rejectPTrip", function(request, response) {
 
	if(request.user){

 			  var PlannedTrip = Parse.Object.extend("PlannedTrips");
		      var pTrip = new PlannedTrip();
		      pTrip.id = request.params.pTrip_objid;

		      pTrip.addUnique("rejected_users",request.user.id);
		      // Save
		      pTrip.save();

		      response.success("Rejection Processed Successfully");

	}else{
		response.error("You are not a logged in user");
    }  

});




//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("finishPTrip", function(request, response) {
  
if(request.user){

	  var var_time = new Date();
	  
	  var ChildPTrip = Parse.Object.extend("ChildPTrips");

      var childPTrip = new ChildPTrip();
      childPTrip.id = request.params.childPTrip_objid;
	  	  
	  childPTrip.set("endDate", var_time);
	  childPTrip.set("trip_status", 2);
	  childPTrip.save(null, {success: function(result){
			
			var obj_response_success = { "trip_id" : result.id, "message" : "Trip is Successfully finished !"};
	        response.success(obj_response_success);	        
	      },
	      error : function(error){                                        
	      		console.log("error is ");
	      	    console.log(error);
	            response.error(error);
	        }
	 }); //save code ends here

}else{
	response.error("You are not a logged in user");
}


});


//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("viewMyPTrips", function(request, response) {
  
  	  var query = new Parse.Query("ChildPTrips");
      query.include("pTrip_obj");
      query.equalTo("user_obj",request.user);
      query.notEqualTo("trip_status", 2);
      query.find({
        success: function(results) {
           response.success(results);
        },
        error: function() {
          response.error("Some Issue with viewmytrips api");
        }
      });            

});


//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("viewInvitedPTrips", function(request, response) {

  	  var query = new Parse.Query("PlannedTrips");
      query.containedIn("invited_users", [request.user.id]);
      query.equalTo("isActive",true);
      query.find({
        success: function(results) {
           response.success(results);
        },
        error: function() {
          response.error("Some Issue with view invitedtrips api");
        }
      });             
});



//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("viewInvitedTripDetails", function(request, response) {

  var PlannedTrip  = Parse.Object.extend("PlannedTrips");
  var query   = new Parse.Query(PlannedTrip);
  query.include("user_obj");
  query.get(request.params.pTrip_objid, {
    success: function(result){ 

    	  var query = new Parse.Query(Parse.User);
		  query.select("username","name","profile_pic");
		  query.containedIn("objectId",result.get("invited_users"));
		  query.limit(result.get("invited_users").length);
		  query.find().then(function(results_users){

			    if(results_users.length){

			    var array_of_users = new Array();

                  for(var i = 0; i < results_users.length; i++)
                  { 
                        // temp_array_of_users[i].addUnique("user_notifications",offer_obj);
                        // temp_array_of_users[i].increment("notification_count",1);
                        // array_of_users[i] = temp_array_of_users[i];

                        if(result.get("accepted_users").indexOf(results_users[i].id) > -1){

                        	var obj_user_status = {"userid":results_users[i].id, "profile_pic":results_users[i].get("profile_pic"), "username": results_users[i].get("username"), "name": results_users[i].get("name"), "status":"accepted"};
                        	array_of_users[i] = obj_user_status;

                        }else if(result.get("rejected_users").indexOf(results_users[i].id) > -1){

                        	var obj_user_status = {"userid":results_users[i].id, "profile_pic":results_users[i].get("profile_pic"), "username": results_users[i].get("username"), "name": results_users[i].get("name"), "status":"rejected"};
                        	array_of_users[i] = obj_user_status;

                        }else{

                        	var obj_user_status = {"userid":results_users[i].id, "profile_pic":results_users[i].get("profile_pic"), "username": results_users[i].get("username"), "name": results_users[i].get("name"), "status":"pending"};
                        	array_of_users[i] = obj_user_status;
                        }
                        
                   }

                   array_of_users = _.sortBy(array_of_users, function(o) { return o.status; });

                   var obj_response_success = { "trip_details" : result, "invitees" : array_of_users};
                   response.success(obj_response_success);

			    }else{

			      var obj_response_success = { "trip_details" : result, "invitees" : []};
                  response.success(obj_response_success);

			    }
		  },function(error){
		    res.error(error.message);
		  });
    },
    error: function(error) {
      
      response.error("Error getting MyTripDetails"); 
      
    }
  });  

});



//This is the function to create a new trip of type Plan A Trip
Parse.Cloud.define("viewMyTripDetails", function(request, response) {

  var ChildPTrip  = Parse.Object.extend("ChildPTrips");
  var query   = new Parse.Query(ChildPTrip);
  query.include("pTrip_obj.user_obj");
  query.get(request.params.childPTrip_objid, {
    success: function(result){ 

    	  var query = new Parse.Query(Parse.User);
		  query.select("username","name","profile_pic");
		  query.containedIn("objectId",result.get("pTrip_obj").get("invited_users"));
		  query.limit(result.get("pTrip_obj").get("invited_users").length);
		  query.find().then(function(results_users){

			    if(results_users.length){

			    var array_of_users = new Array();

                  for(var i = 0; i < results_users.length; i++)
                  { 
                        // temp_array_of_users[i].addUnique("user_notifications",offer_obj);
                        // temp_array_of_users[i].increment("notification_count",1);
                        // array_of_users[i] = temp_array_of_users[i];

                        if(result.get("pTrip_obj").get("accepted_users").indexOf(results_users[i].id) > -1){

                        	var obj_user_status = {"userid":results_users[i].id, "username": results_users[i].get("username"), "name": results_users[i].get("name"), "status":"accepted"};
                        	array_of_users[i] = obj_user_status;

                        }else if(result.get("pTrip_obj").get("rejected_users").indexOf(results_users[i].id) > -1){

                        	var obj_user_status = {"userid":results_users[i].id, "username": results_users[i].get("username"), "name": results_users[i].get("name"), "status":"rejected"};
                        	array_of_users[i] = obj_user_status;

                        }else{

                        	var obj_user_status = {"userid":results_users[i].id, "username": results_users[i].get("username"), "name": results_users[i].get("name"), "status":"pending"};
                        	array_of_users[i] = obj_user_status;
                        }
                        
                   }

                   array_of_users = _.sortBy(array_of_users, function(o) { return o.status; });

                   var obj_response_success = { "trip_details" : result, "invitees" : array_of_users};
                   response.success(obj_response_success);

			    }else{

			      var obj_response_success = { "trip_details" : result, "invitees" : []};
                  response.success(obj_response_success);

			    }
		  },function(error){
		    res.error(error.message);
		  });
      	
    },
    error: function(error) {

      response.error("Error getting MyTripDetails"); 
      
    }
  });    
  
});